const express = require("express");
const { graphqlHTTP } = require("express-graphql");
const { buildSchema } = require("graphql");
const fs = require("fs");

const callback = (err, name, content) => {
  if (err) console.log(err);
  else {
    DB[name] = content[name];
  }
};

const readGraph = (name) => {
  fs.readFile(`./src/components/${name}.json`, (err, fd) => {
    if (err) console.log(err);
    else {
      console.log("File opened succesfully!");
      callback(null, name, JSON.parse(fd));
    }
  });
};

const saveGraph = (content) => {
  const obj = JSON.parse(content);
  const name = Object.getOwnPropertyNames(obj)[0];

  fs.writeFileSync(`./src/components/${name}.json`, content);
  return "1";
};

const schema = buildSchema(`
    type Query {
        header: [[String]]
        set(name: String, value: String): String
        save: String
    }
`);

const root = {
  header: () => DB.header,
  set: (properties) => {
    if (Object.getOwnPropertyNames(DB).includes(properties.name)) {
      DB[properties.name] = JSON.parse(properties.value);
      root.save();
      return 1;
    } else {
      return 0;
    }
  },
  save: () => {
    for (let name in DB) {
      saveGraph(JSON.stringify({ [name]: DB[name] }));
    }
  },
};

const DB = {
  header: [],
};

const app = express();
const port = 7878;

app.use(
  "/graphql",
  graphqlHTTP({
    schema,
    rootValue: root,
    graphiql: true,
  })
);

for (let name in DB) {
  readGraph(name);
}

app.listen(port);

console.log(`Running a GraphQL API server at http://localhost:${port}/graphql`);
