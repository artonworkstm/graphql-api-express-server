# 🚀 GraphQL Express API Server 🚀

The Server creates **automated backups** of the database.

## To build

```
yarn build
```

or

```
npm run build
```

## To Start Development Server

```
yarn build
```

or

```
npm run build
```
